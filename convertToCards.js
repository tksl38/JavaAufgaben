/*******************************************************************************
 *
 * This script converts the Java problems from Prof. Dr. Dominikus Herzberg
 * (https://git.thm.de/dhzb87/JavaAufgaben/blob/master/README.md) to flashcards
 * for the .cards-project (https://git.thm.de/arsnova/flashcards).
 *
 * It takes all .adoc files on the same directory level starting with a number
 * and parses them according to the following example:
 *
 * = <chapter name of the book>
 * <Author Names>
 * :sourcedir: <directory with source files for this file>
 * :imagedir: <directory with image files for this file>
 * == <1. chapter title>
 * === <1. subchapter title>
 * === <2. subchapter title>
 * ==== <subsubchapter title>
 * == <2. chapter title>
 *
 * If a chapter has subchapters, only the subchapters are used to create cards,
 * else the chapters are used. If a subchapter is available, everything
 * between the chapter start and the subchapter start is omitted.
 * Subsubchapters are parsed as headers inside the subchapter card.
 * The example produces 3 cards:
 * - 1. subchapter
 * - 2. subchapter
 * - 2. chapter
 *
 * The solutions inside (sub-)chapters are used to fill a cards backside
 * (solution). Everything between "preDetailsSolution" and "postDetails" will be
 * used.
 *
 * - images:
 * In the imagedir need to be uploaded somewhere publicly accessable. Currently,
 * they are uploaded to arsnova-uploads.mni.thm.de via Continuous Integration
 * Pipeline.
 *
 * - sources:
 * Code files are just read and pasted into the card
 *
 *
 * Cards uses Markdeep (https://casual-effects.com/markdeep/features.md.html),
 * so this script converts the text markup accordingly.
 ******************************************************************************/

var fs = require('fs');
var path = require('path');

var asciDocFiles = [];

var cards = [];

// get all asciidoc files that need conversion
var allFiles = fs.readdirSync('.');
for (var i = 0; i < allFiles.length; i++) {
  var filename = path.join('./', allFiles[i]);
	var regex = /^\d.*?\.adoc$/;
  if (regex.test(filename)) {
		asciDocFiles.push(allFiles[i]);
	}
};

var date = new Date(new Date().toUTCString().substr(0, 25))
// convert files to cards
asciDocFiles.forEach(function (file) {
  var fileContent = fs.readFileSync(file, 'utf8').toString();

  // splits content into prelude and potential cards
  var preludePtr = fileContent.indexOf("== ");
  var prelude = fileContent.substr(0, preludePtr).split('\n');
  var cardContent = fileContent.substr(preludePtr);

  var info = {
    imagedir: "",
    sourcedir: "",
    texts: []
  };

  for (var i = 0; i < prelude.length; i++) {
    // remember some important info....
    var imagedirPtr = prelude[i].indexOf(":imagedir: ");
    if (imagedirPtr > -1) {
      info.imagedir = prelude[i].substr(imagedirPtr + ":imagedir: ".length);
    }
    var sourcedirPtr = prelude[i].indexOf(":sourcedir:");
    if (sourcedirPtr > -1) {
      info.sourcedir = prelude[i].substr(sourcedirPtr + ":sourcedir: ".length);
    }
  }

  var texts = [];
  // ugly
  cardContent = "\n" + cardContent;
  var chapterStartPtr = cardContent.indexOf("\n== ");
  var nextChapterStartPtr;
  var endOfContentPtr = cardContent.length - 1;
  while (chapterStartPtr > -1 && chapterStartPtr < endOfContentPtr) {
    nextChapterStartPtr = cardContent.indexOf("\n== ", chapterStartPtr + "\n== ".length);
    if (nextChapterStartPtr === -1) {
      nextChapterStartPtr = endOfContentPtr;
    }

    var subChapterStartPtr = cardContent.indexOf("\n=== ", chapterStartPtr + "\n=== ".length);
    var nextSubChapterStartPtr;

    if (subChapterStartPtr > -1 && subChapterStartPtr < nextChapterStartPtr) {
      while (subChapterStartPtr > -1 && subChapterStartPtr < nextChapterStartPtr) {
        nextSubChapterStartPtr = cardContent.indexOf("\n=== ", subChapterStartPtr + "\n=== ".length);
        if (nextSubChapterStartPtr === -1 || nextSubChapterStartPtr >= nextChapterStartPtr) {
          nextSubChapterStartPtr = nextChapterStartPtr;
        }

        var subTitleEndPtr = cardContent.indexOf('\n', subChapterStartPtr + "\n=== ".length);
        var subTitleLength = subTitleEndPtr - (subChapterStartPtr + "\n=== ".length);
        var subCardSubject = cardContent.substr(subChapterStartPtr + "\n=== ".length, subTitleLength);

        var txt = cardContent.substr(subTitleEndPtr + '\n'.length, nextSubChapterStartPtr - (subTitleEndPtr + '\n'.length));

        info.texts.push({subject: subCardSubject, text: txt});

        subChapterStartPtr = nextSubChapterStartPtr;
      }
    } else {
      var titleEndPtr = cardContent.indexOf('\n', chapterStartPtr + '\n'.length);
      var titleLength = titleEndPtr - (chapterStartPtr + "\n== ".length);
      var cardSubject = cardContent.substr(chapterStartPtr + "\n== ".length, titleLength);

      var cardText = cardContent.substr(titleEndPtr + '\n'.length, nextChapterStartPtr - (titleEndPtr + '\n'.length));

      info.texts.push({subject: cardSubject, text: cardText});
    }

    chapterStartPtr = nextChapterStartPtr;
  }

  for (var i = 0; i < info.texts.length; i++) {
  	var newCard = {
  		subject: "",
  		front: "",
      back: "",
  		centerTextElement: [false, false, false, false, false, false],
  		date: new Date(),
  		learningGoalLevel: 0,
  		backgroundStyle: 1,
  		learningIndex: 0,
  		learningUnit: 0,
  		originalAuthorName: {
  			title: "",
  			birthname: "",
  			givenname: ""
  		}
  	};

    var text = info.texts[i].text.split('\n');
    var cardText = [];
    var cardSolution = [];
    var currentlyInSolution = false;
    var currentTipStatus = "none";
    var skipText = false;
  	for (var j = 0; j < text.length; j++) {
      skipText = false;
  		// replace itemlist markup
  		// *  -->  -
  		if (text[j][0] == "*") {
  			text[j] = "-" + text[j].substr(1);
  		}

  		// replace code markup
  		text[j] = text[j].replace("----", "~~~~~~~~~~");

      // filter source code highlighting
      if (text[j].indexOf("[source,") > -1) {
        skipText = true;
      }

      // replace web links
      var linkRgx = /(http:\/\/|https:\/\/)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?\[*\]/;
      if (text[j].search(linkRgx) > -1) {
        var linkStartPtr = text[j].search(linkRgx);
        var linkEndPtr = text[j].indexOf("[", linkStartPtr);
        var linkTitleEndPtr = text[j].indexOf("]", linkEndPtr);

        var url = text[j].substr(linkStartPtr, linkEndPtr - linkStartPtr);
        var linkTitle = text[j].substr(linkEndPtr + 1, linkTitleEndPtr - (linkEndPtr + 1));
        var newLink = "[" + linkTitle + "]" + "(" + url + ")";

        text[j] = text[j].substr(0, linkStartPtr) + newLink + text[j].substr(linkTitleEndPtr + 1);
      }

  		// replace images with uploaded ones
  		// image::{imageDir}/<imagename>[caption="<caption>", title="<title>"]
  		// -->
  		// ![<title>](https://arsnova-uploads.mni.thm.de/java-aufgaben/<imageDir>/<imagename>)
  		if (text[j].indexOf("image::") > -1) {
  			var imagenamePtr = text[j].indexOf("{imageDir}/") + "{imageDir}/".length;
        var imagenameEndPtr = text[j].indexOf("[caption");
        var imagename = text[j].substr(imagenamePtr, imagenameEndPtr - imagenamePtr);

        var titlePtr = text[j].indexOf("title=\"") + "title=\"".length;
        var titleEndPtr = text[j].indexOf("\"]");
        var imageTitle = text[j].substr(titlePtr, titleEndPtr - titlePtr);

        text[j] = "![" + imageTitle + "](https://arsnova-uploads.mni.thm.de/java-aufgaben/" + info.imagedir + "/" + imagename + ")";
  		}

  		// don't forget about the code
      if (text[j].indexOf("include::{sourcedir}") > -1) {
        // the one is because of \ / bs
        var filenameStartPtr = text[j].indexOf("{sourcedir}") + "{sourcedir}".length + 1;
        var filenameEndPtr = text[j].indexOf("[", filenameStartPtr);
        var filename = text[j].substr(filenameStartPtr, filenameEndPtr - filenameStartPtr);

        var codeContent = fs.readFileSync("./" + info.sourcedir + "/" + filename, 'utf8').toString();

        text[j] = codeContent;
      }

      // tips
      if (text[j] === "[TIP]") {
        currentTipStatus = "start";
        text[j] = "!!! Tip";
      }
      if (text[j] === "[IMPORTANT]") {
        currentTipStatus = "start";
        text[j] = "!!! WARNING";
      }
      if (currentTipStatus === "tip" && text[j] !== "====") {
        text[j] = "   " + text[j];
      }
      if (currentTipStatus === "tip" && text[j] === "====") {
        currentTipStatus = "none";
        skipText = true;
      }
      if (currentTipStatus === "start" && text[j] === "====") {
        currentTipStatus = "tip";
        skipText = true;
      }
      // convert ==== to a header
      if (currentTipStatus === "none" && text[j].indexOf("==== ") > -1) {
        var titleStartPtr = text[j].indexOf("==== ") + "==== ".length;
        var title = text[j].substr(titleStartPtr);
        text[j] = "# " + title;
      }

      // solutions.
      if (text[j].indexOf("include::postDetails") > -1) {
        currentlyInSolution = false;
        skipText = true;
      }
      if (currentlyInSolution) {
        if (text[j].indexOf("[source,") === -1) {
          cardSolution.push(text[j]);
        }
        skipText = true;
      }
      if (text[j].indexOf("include::preDetailsSolution") > -1) {
        currentlyInSolution = true;
        skipText = true;
      }

      if (!skipText) {
        cardText.push(text[j]);
      }
		}

  	newCard.front = cardText.join('\n');
    newCard.back = cardSolution.join('\n');
  	newCard.subject = info.texts[i].subject;
  	newCard.date = date;

  	cards.push(newCard);
  }
});

fs.writeFile("./java-aufgaben.json", JSON.stringify(cards), (err) => {
    if (err) {
        console.error(err);
        return;
    };
});
