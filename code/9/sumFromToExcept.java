int sumFromTo(int x, int y) throws IllegalArgumentException { // <1>
  if(x > y) {
    throw new IllegalArgumentException("x must be less or equal to y!");
  }
  if(y == x) return 0;
  return x + sumFromTo(x+1, y);
}