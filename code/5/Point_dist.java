class Point {
  int x;
  int y;
  Point(int x, int y) {
    this.x = x;
    this.y = y;
  }
  double dist(Point other) {
    return Math.sqrt(Math.pow(this.x - other.x, 2) 
                   + Math.pow(this.y - other.y,2));
  }
}