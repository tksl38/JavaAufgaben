Stack createDeck() {
  Stack s = null;
  // four suits: spades, hearts, diamonds, clubs
  for(int i = 0; i < 4; i++) {
    // for each suit: 7,8,9,10,jack (10),queen (10),king (10),ace (11)
    for(int v : new int[]{7,8,9,10,10,10,10,11}) {
      s = new Stack(v, s);
    }
  }
  return s;
}

import javax.swing.JOptionPane;
Stack deck = shuffled(createDeck());

class Player {
  boolean playing;
  int sum;
  Player() {
    playing = true;
    sum = 0;
  }
  void play(Stack deck) {
    sum += deck.pop();
  }
  boolean canPlay() {
    return playing && sum < 21;
  }
}

Player player = new Player();
Player opponent = new Player();
while(player.canPlay() || opponent.canPlay()) {
  if(player.canPlay()) {
    player.play(deck);
    System.out.printf("Eigene Summe: %d\n", player.sum);
    int choice = JOptionPane.showConfirmDialog(null, "Nächste Karte?");
    player.playing = choice == JOptionPane.YES_OPTION;
  }
  if (opponent.canPlay()) {
    System.out.printf("Der Gegner nimmt noch eine Karte.\n");
    opponent.play(deck);
    opponent.playing = Math.random() > opponent.sum / 21.0;
    if (!opponent.playing) {
      System.out.printf("Der Gegner steigt aus.\n");
    }
  }
}

System.out.printf("Gegnerische Summe: %d\n", opponent.sum);
if (player.sum > 21) {
  System.out.printf("Leider verloren!\n");
} else if (player.sum == 21) {
  System.out.printf("Genau 21, super!\n");
} else if (player.sum > opponent.sum || opponent.sum > 21) {
  System.out.printf("Gewonnen!\n");
} else {
  System.out.printf("Leider verloren!\n");
}