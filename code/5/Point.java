class Point {
  int x;       // <1>
  int y;
  Point(int x, int y) {  // <2>
    this.x = x;
    this.y = y;
  }
}