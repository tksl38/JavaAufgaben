boolean isPalindrome(String word) {
  if (word.length() <= 1) return true;
  String s = word.toLowerCase();
  int last = s.length() - 1;
  return s.charAt(0) == s.charAt(last) && isPalindrome(s.substring(1,last));
}

assert isPalindrome("a");
assert isPalindrome("");
assert isPalindrome("Otto");
assert isPalindrome("Lagerregal");
assert isPalindrome("Reliefpfeiler");
assert !isPalindrome("Papa");
assert !isPalindrome("Test");