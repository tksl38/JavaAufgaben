int add(int x, int y) {
  if(y == 0) return x;
  return add(++x, --y);   //    <1>
}

int mul(int x, int y) {
  if(y == 0) return 0;
  else if (y == 1) return x;
  return add(x, mul(x, --y));
}