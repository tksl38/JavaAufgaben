String tens_v1(int n) { // erste Variante
    if (n <= 9) return ones(n); // gelöst!
    int tens = n / 10; // Zehnerstellen
    int ones = n % 10; // Einerstellen
    if (ones == 0) {   // Zehnerzahl: 10, 20, ...
        switch (n) {
            case 10: return "zehn";
            case 20: return "zwanzig";
            case 30: return "dreißig";
            case 60: return "sechzig";
            case 70: return "siebzig";
            default: return ones(tens) + "zig";
        }
    }
    if (n == 11) return "elf";
    if (n == 12) return "zwölf";
    if (n == 16) return "sechzehn";
    if (n == 17) return "siebzehn";
    if (n <= 19) return ones(ones) + tens(10);
    if (ones == 1) return "einund" + tens(tens * 10);
    return ones(ones) + "und" + tens(tens * 10);
}

/* Diese finale Variante nutzt aus, dass sich verkürzte
   Sprechweisen mit substring ableiten lassen.
*/
String tens(int n) {
    // n ist Zahl von 0 bis 99
    if (n <= 9) return ones(n);
    int tens = n / 10;
    int ones = n % 10;
    if (ones == 0) {
        if (n == 10) return "zehn";
        if (n == 20) return "zwanzig";
        if (n == 30) return "dreißig";
        return ones(tens).substring(0,4) + "zig";
    }
    if (n == 11) return "elf";
    if (n == 12) return "zwölf";
    if (n <= 19) return ones(ones).substring(0,4) + "zehn";
    if (ones == 1) return "einund" + tens(tens * 10);
    return ones(ones) + "und" + tens(tens * 10);
}

void test_tens() { // Test funktioniert wg. Umlauten nicht :-(
    for(int i = 0; i <= 99; i++) {
        if (!tens(i).equals(tens_v1(i))) throw new AssertionError();
    }
}

/* Hunderterstellen zu vertexten ist nun leicht.
   Interessant ist, dass diese Lösung eine Zahl
   wie 1901 in "neunzehnhunderteins" umwandelt. 
*/
String hundreds(int n) {
    if (n < 100) return tens(n);
    int hundreds = n / 100; // Hunderterstellen
    int rest = n % 100;     // "Restzahl""
    String s = (hundreds == 1 ? "ein" : tens(hundreds)) + "hundert";
    if (rest == 0) return s;
    return s + tens(rest);
}

/* Der Code für Tausender ähnelt dem für
   Hunderter sehr.
*/
String thousends(int n) {
    if (n < 1000) return hundreds(n);
    int thousends = n / 1000;
    int rest = n % 1000;
    String s = (thousends == 1 ? "ein" : hundreds(thousends)) + "tausend";
    if (rest == 0) return s;
    return s + " " + hundreds(rest);
}

/* Spätestens jetzt ist klar: es gibt ein
   sich wiederholdendes Code-Schema!
*/
String millions(int n) {
    if (n < 1_000_000) return thousends(n);
    int millions = n / 1_000_000;
    int rest = n % 1_000_000;
    String s;
    if (millions == 1) s = "eine Million";
    else s = thousends(millions) + " Millionen";
    if (rest == 0) return s;
    return s + " " + thousends(rest);
}

/* In 1000er Schritten verändert sich der Zahlenname,
   d.h. alle drei Zahlenstellen weiter kommt ein
   neuer Zahlenbezeichner ins Spiel. Nach geringer
   Vorarbeit findet sich das obige Schema hier kodiert.
*/
String number2text(long n) {
    // https://de.wikipedia.org/wiki/Zahlennamen
    if (n < 100) return tens((int) n);
    String pattern[][] = {{"einhundert", "hundert"},
                          {"eintausend", "tausend"},
                          {"eine Million", " Millionen"},
                          {"eine Milliarde", " Milliarden"},
                          {"eine Billion", " Billionen"},
                          {"eine Billiarde", " Billiarden"},
                          {"eine Trillion", " Trillionen"}};
    int index = ((int) Math.log10(n)) / 3;
    if (index >= pattern.length) index = pattern.length - 1;
    long base = index == 0 ? 100 : (long) Math.pow(10, 3*index);
    long prefix = n / base;
    long rest = n % base;
    String s;
    if (prefix == 1) s = pattern[index][0];
    else s = number2text(prefix) + pattern[index][1];
    if (rest == 0) return s;
    return s + " " + number2text(rest);
}

/* Ob man Leerzeichen anders setzt und Großschreibung verwendet,
   das lässt sich diskutieren und entsprechend anpassen.
*/
