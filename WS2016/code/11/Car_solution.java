class Car {
  float speed;
  void accelerate(float amount) throws TooFastException {
    if (amount < 0 || amount > 10) {
      String msg = "Acceleration " + amount + " was not between 0 and 10.";
      throw new IllegalArgumentException(msg);
    } else if (speed + amount >= 300) {
      String msg = "Speed " + (speed + amount) + " would be too fast.";
      throw new TooFastException(msg);
    }
    speed += amount;
  }
}

class TooFastException extends Exception {
  TooFastException(String msg) { super(msg); }
}
