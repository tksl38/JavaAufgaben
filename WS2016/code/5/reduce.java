int gcd(int a, int b) {
    int min = Math.min(Math.abs(a), Math.abs(b));
    for(int d = min; d >= 2; d--) {
        if (a % d == 0 && b % d == 0) return d; 
    }
    return 1;
}