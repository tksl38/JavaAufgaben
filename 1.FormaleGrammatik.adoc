= Formale Sprachen und Grammatiken
Dominikus Herzberg
:toc: left
:toctitle: Inhaltsverzeichnis
:toclevels: 4
:icons: font
:imagedir: images
:stylesheet: italian-pop.asciidoc.css

include::prelude.adoc[]

== Formale Sprachen

Welche Aussagen sind richtig?

=== Morsecode

Mit Hilfe des https://de.wikipedia.org/wiki/Morsezeichen[Morsecodes] (z.B. als Blinkzeichen auf See) kann man Textbotschaften übermitteln. Der Morsecode ist eine

- [ ] formale Sprache.
- [ ] natürliche Sprache.

include::preDetailsSolution.adoc[]
- [x] formale Sprache.
include::postDetails.adoc[]

=== Esperanto

https://de.wikipedia.org/wiki/Esperanto[Esperanto] ist eine sogenannte Plansprache, eine einfache Sprache, die helfen soll, dass sich Menschen auf der ganzen Welt miteinander verständigen können. Das Besondere an Esperanto ist, dass die Grammatik einfachen und klaren Regeln folgt.

- [ ] Esperanto ist eine formale Sprache.
- [ ] Esperanto ist eine natürliche Sprache.

include::preDetailsSolution.adoc[]
- [x] Esperanto ist eine natürliche Sprache.
include::postDetails.adoc[]

=== Syntax und Semantik

- [ ] Die Syntax definiert gültige Zeichenfolgen.
- [ ] Die Semantik definiert gültige Zeichenfolgen.
- [ ] Die Syntax definiert die Bedeutung einer Zeichenfolge.
- [ ] Die Semantik definiert die Bedeutung einer Zeichenfolge.

include::preDetailsSolution.adoc[]
- [x] Die Syntax definiert gültige Zeichenfolgen.
- [x] Die Semantik definiert die Bedeutung einer Zeichenfolge.
include::postDetails.adoc[]

=== Mittel zur Beschreibung von Grammatiken

Welches Symbol in einer Grammatik-Beschreibung repräsentiert die ...

* Wiederholung?
* Alternative?
* Option?
* Gruppe?

include::preDetailsSolution.adoc[]
Die Lösung orientiert sich an der https://de.wikipedia.org/wiki/Erweiterte_Backus-Naur-Form[EBNF]:

* Wiederholung durch geschweifte Klammern `{` `}` (gar nicht oder beliebig oft)
* Alternative durch senkrechten Strich `|`
* Option durch eckige Klammern `[` `]` (gar nicht oder einmal)
* Gruppe durch runde Klammern `(` `)`

Die Notation der Java-Syntax (siehe https://docs.oracle.com/javase/specs/jls/se9/html/jls-2.html#jls-2.4[Kap. 2.4] der _Java Language Specification_, JLS) hält sich daran, lediglich Alternativen werden zeilenweise oder durch den Hinweis "_one of_" dargestellt.
include::postDetails.adoc[]

=== Was ist eine Produktionsregel?

Definieren Sie, was eine Produktionsregel ist.

include::preDetailsSolution.adoc[]
Eine Produktionsregel ist eine mit einem Namen versehene Regel, die angibt, wie aus Terminalsymbolen und/oder anderen Regeln ein gültiges Satzfragment einer formalen Sprache gebildet wird. [Eine formale Definition findet sich z.B. in der https://de.wikipedia.org/wiki/Produktionsregel[Wikipedia].]
include::postDetails.adoc[]

=== Namen

In den Produktionsregeln kommen Namen vor

- [ ] zur Bezeichnung von Regeln
- [ ] zur Suche nach Gleichheiten
- [ ] zum Verweis auf eine Regel
- [ ] zur Klarstellung von Regeln

include::preDetailsSolution.adoc[]
- [x] zur Bezeichnung von Regeln
- [x] zum Verweis auf eine Regel
include::postDetails.adoc[]

== Grammatiken

[IMPORTANT]
====
Die Lösungen zu den Aufgaben halten es nicht so strikt, aber in Prüfungen gelten meist zwei zusätzliche Bedingungen, die Ihre Lösung erfüllen muss:

* Jedes Terminalsymbol darf nur einmal verwendet werden.
* Die Anzahl der Produktionsregeln ist so klein wie möglich zu halten.

Der Grund ist, dass dann meist nur noch eine gültige Lösung zu finden ist, was die Korrektur von solchen Aufgaben vereinfacht.
====

=== Zeitangabe

Geben Sie die syntaktischen Regeln an, die eine gültige Darstellung einer Uhrzeit im 24h-Format `HH:MM` definieren! `HH` steht für die zweistellige Stunden-, `MM` für die zweistellige Minutenangabe.

=== Datumsangabe

Geben Sie eine formale Definition der Angabe eines Datums in der Form `YYYY-MM-DD` an. `YYYY` steht für die vierstellige Angabe der Jahreszahl (_years_), `MM` für die zweistellige Monats- (_months_) und `DD` für die zweistellige Tagesangabe (_days_). Hinweis: Berücksichtigen Sie die Abhängigkeit der Tage vom Monat, nicht jedoch zusätzlich vom Jahr.

include::preDetailsSolution.adoc[]
----
digit1-9: "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9"

digit: "0" | digit1-9
----

----
YYYY: digit digit digit digit
----

----
29Days: "0" digit1-9
      | "1" digit
      | "2" digit
----

----
30Days: 29Days | "30"
----

----
31Days: 30Days | "31"
----

----
Mon31: "01" | "03" | "05" | "07" | "08" | "10" | "12"
----

----
Mon30: "04" | "06" | "09" | "11"
----

----
YYYY-MM-DD: YYYY "-" ( Mon31 "-" 31Days
                     | Mon30 "-" 30Days
                     | "02   "-" 29Days" )
----

Die gezeigte Grammatik hat ein Problem: Sie legt nicht fest, wann der Februar mit 28 und wann mit 29 Tagen zu schreiben ist. Die Entscheidung rechnet man typischerweise der Semantik zu, denn es erfordert Verständis, wann ein Jahr ein Schaltjahr ist. Das können die Schreibregeln für ein Jahr nicht so einfach festlegen. Mehr dazu habe ich Ihnen aufgeschrieben in dem Blog-Beitrag https://denkspuren.blogspot.de/2008/11/syntax-und-semantik.html[Syntax und Semantik].
include::postDetails.adoc[]

=== Ganzzahl

Definieren Sie die Syntax einer Ganzzahl. Das Vorzeichen ist optional.

Beispiele für ungültige Ganzzahlen sind `007`, `-03`, `++24`.

=== Ganzzahl mit Tausendermarkierung

Definieren Sie eine Syntax für ausschließlich positive Ganzzahlen, wobei Tausender-Stellen durch einen Punkt kenntlich gemacht werden _müssen_.

Gültige Beispiele sind: `1.000`, `1.024`, `100.000`, `2.023.999`.

Ungültige Beispiele sind: `1.00`, `0.000`, `0.123`. Verwechseln Sie den Tausenderpunkt nicht mit einem Komma!

include::preDetailsSolution.adoc[]
----
digit0: "0"

digit1-9: "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9"

digit: digit0 | digit1-9
----

----
PostDot: digit digit digit
----

----
PreDot: digit1-9 [ digit ] [ digit ]
----

----
Number: digit0
      | PreDot { "." PostDot }
----
include::postDetails.adoc[]

=== Arithmetischer Ausdruck

Geben Sie die syntaktischen Regeln an für einen arithmetischen Rechenausdruck, in dem Zahlen, die Operatoren `+`, `-`, `*` und `/` vorkommen können und runde Klammern verwendet werden dürfen. Gehen Sie davon aus, dass es eine syntaktische Regel für Zahlen unter dem Namen `int` gibt.

Beispiele für gültige arithmetische Ausdrücke sind: `7`, `(7)`, `2+3`, `2+3*4`, `(2+3)*4`, `(1+(2+3)*4)/7`

Beispiele für ungültige Ausdrücke sind: `+`, `+1`, `2+(3*4`, `((3)`, `2+3*`, `+0`, `-0`.

include::preDetailsSolution.adoc[]
----
expr: number
    | expr op expr
    | "(" expr ")"

op: "+" | "-" | "×" | "/"

nonZeroDigit: "1" | "2" | "3" | "4" | "5"
            | "6" | "7" | "8" | "9"

digit: "0" | nonZeroDigit

sign: "+" | "-"

number: "0"
      | [sign] nonZeroDigit {digits}
----
include::postDetails.adoc[]


=== Römische Zahlen

Entwerfen Sie eine Grammatik für https://de.wikipedia.org/wiki/R%C3%B6mische_Zahlschrift[römische Zahlen] von 1-999.

image::{imageDir}/0.roman.png[caption="Römische Zahlen", title="Römische Zahlen"]
