= Enumerationen
Dominikus Herzberg, Christopher Schölzel
:toc: left
:toctitle: Inhaltsverzeichnis
:toclevels: 4
:icons: font
:stylesheet: italian-pop.asciidoc.css
:source-highlighter: coderay
:sourcedir: code/7

include::prelude.adoc[]

Die Aufgaben in diesem Kapitel üben den Umgang mit Enumerationen (Aufzählungsklassen) ein.

.Mehr ist besser
****
Uns fehlen definitiv noch ein paar gute Aufgaben. Wenn Sie mögen, machen Sie uns Vorschläge!
****

== Rechteverwaltung

Sehen Sie sich den Code zu dem folgenden Rechteverwaltungssystem einmal genauer an:

[source,java]
----
include::{sourcedir}/roles.java[]
----

. Schreiben Sie den Code um, indem Sie einen selbst definierten Enumerationstyp statt einem `int` für das Feld `access` verwenden.
. Erweitern Sie beide Varianten um ein zusätzliches Zugriffslevel für Entwickler (engl. _developer_), für die `canUploadFiles` ebenfalls `true` zurückgeben soll. Bei welcher Variante fällt ihnen die Erweiterung leichter?
. Welche Variante würden sie bevorzugen? Gibt es Probleme, die durch die Benutzung von Enumerationen entstehen, oder die sich durch ihre Benutzung verhindern lassen?

include::preDetailsSolution.adoc[]
[source,java]
----
include::{sourcedir}/roles_solution.java[]
----

Beide Varianten lassen sich leicht erweitern und leider besteht auch bei beiden Varianten das Problem, dass, so wie die Vorgabe definiert ist, bei einer Erweiterung die neue Rolle erst einmal das Recht zum Hochladen von Dateien erhält, ob das nun erwünscht ist oder nicht. Als Programmierer muss man von Hand jede `switch`- oder `if`-Anweisung überprüfen in der der geänderete Typ vorkommt, um sicher zu gehen, dass durch die Änderung keine ungewünschten Nebenwirkungen entstehen.

Ein Unterschied besteht jedoch darin, dass ein Objekt vom Typ `UserInt` prinzipiell 2^32^ unterschiedliche Zugriffsstufen (_access level_) besitzen kann, mit denen man umgehen können muss, während Objekte vom Typ `UserEnum` tatsächlich nur die drei explizit definierten Zugriffssstufen besitzen können.

Gerade für einen solchen Fall, in dem die Sicherheit des Codes relevant ist, wären also Enumerationstypen zu bevorzugen.
include::postDetails.adoc[]

== Organische Enums

Sie wollen ein Programm für eine Anwendung in der organischen Chemie schreiben. Um die chemischen Elemente möglichst platzsparend zu speichern entscheiden Sie sich, diese als `enum` zu implementieren.

Für ihre Anwendung reicht es, wenn sie die folgenden Elemente darstellen und Name, Symbol und Ordnungszahl auslesen können.

[options="header"]
|====================================
|Name        | Symbol | Ordnungszahl
|Wasserstoff | H      | 1
|Kohlenstoff | C      | 6
|Stickstoff  | N      | 7
|Sauerstoff  | O      | 8
|Phosphor    | P      | 15
|Schwefel    | S      | 16
|====================================

Implementieren Sie den Datentyp `Element` als `enum`, so dass das Symbol als Bezeichner der Enum-Werte dient. Die natürliche Ordnung von `Element`-Objekten, soll durch die Ordnungszahl bestimmt sein.
(D.h. eine Sortierung z.B. mit `Arrays.sort` muss nach aufsteigender Ordnungszahl erfolgen.)

include::preDetailsSolution.adoc[]
----
public enum Element {
  H("Wasserstoff", 1), C("Kohlenstoff", 6), N("Stickstoff", 7),
  O("Sauerstoff", 8), P("Phosphor", 15), S("Schwefel", 16);
  private final int ord;
  private final String name;
  Element(String name, int ord) {
    this.ord = ord;
    this.name = name;
  }
  public int getOrd() {
    return ord;
  }
  public String getName() {
    return name;
  }
}
----
include::postDetails.adoc[]
